<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "facturas".
 *
 * @property int $idfactura
 * @property int $cif
 * @property string $fecha
 * @property int $total
 *
 * @property Detalles[] $detalles
 * @property Camiseta[] $codigos
 * @property Proveedores $cif0
 */
class Facturas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facturas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cif', 'total'], 'integer'],
            [['fecha'], 'safe'],
            [['cif'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['cif' => 'cif']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfactura' => 'Idfactura',
            'cif' => 'Cif',
            'fecha' => 'Fecha',
            'total' => 'Total',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(Detalles::className(), ['idfactura' => 'idfactura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigos()
    {
        return $this->hasMany(Camiseta::className(), ['codigo' => 'codigo'])->viaTable('detalles', ['idfactura' => 'idfactura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCif0()
    {
        return $this->hasOne(Proveedores::className(), ['cif' => 'cif']);
    }
    public function afterFind(){
       parent::afterFind();
       $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y'); // 2014-10-06
    }
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d'); // 2014-10-06
          return true;
       }
    
    
    }
