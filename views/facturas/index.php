<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facturas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facturas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Facturas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idfactura',
            'cif',
            'fecha',
            'total',
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {detalles}',
            'buttons' => [
                'detalles' => function ($url,$model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-book"></span>', 
                        ['detalles/detalles',  'id'=>$model->idfactura]);
                },
                ],
            ],  
            
            
        ],
    ]); ?>


</div>
