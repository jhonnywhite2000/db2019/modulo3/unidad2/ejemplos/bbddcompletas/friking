<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Factura */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="factura-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cif')->textInput() ?>

    <?php
    // Highlight today, show today button, change date format
    echo '<label class="control-label">Fecha</label>';
    echo DatePicker::widget([
    'model' => $model, 
    'attribute' => 'fecha',
    'options' => ['placeholder' => 'Intro  la fecha...'],
    'pluginOptions' => [
        'todayHighlight' => true,
        'todayBtn' => true,
        'format' => 'yyyy/mm/dd',
        'autoclose' => true,
    ]
    ]);
    ?>
    <?= $form->field($model, 'total')->textInput() ?>
    
    
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
