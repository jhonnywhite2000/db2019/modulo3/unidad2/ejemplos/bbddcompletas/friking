<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Camisetas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camiseta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Camiseta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo',
            'precio',
            'talla',
            'descripcion',
             [
               'label'=>'foto',
                'format'=>'raw',
                'value'=>function($model)
                        {
                        return Html::img('@web/imgs/' . $model->imagen, ['class' => 'img-thumbnail', 'width'=>
                            '50px']);
                        }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
