
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Camiseta */

$this->title = 'Update Camiseta: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Camisetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="camiseta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
