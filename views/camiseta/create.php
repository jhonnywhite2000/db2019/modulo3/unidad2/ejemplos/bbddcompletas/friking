<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Camiseta */

$this->title = 'Create Camiseta';
$this->params['breadcrumbs'][] = ['label' => 'Camisetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camiseta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
