<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalles de la factura' .$factura->idfactura;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalles-index">

    <h1><?= Html::encode($this->title) ?></h1>
   <?= DetailView::widget([
        'model' => $factura,
        'attributes' => [
            'idfactura',
            'cif',
            'cif0.nombre',/*De la relacion uno a uno de los eventos*/
            'fecha',
            'total',
        ],
    ]) ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iddetalle',
            'codigo',
            'idfactura',
            'cantidad',
            'codigo0.precio'
            
        ],
    ]); ?>


</div>
